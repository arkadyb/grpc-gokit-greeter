package client

import (
	"bitbucket.org/arkadyb/grpc-gokit-greeter/endpoints"
	"bitbucket.org/arkadyb/grpc-gokit-greeter/endpoints/hello"
	"bitbucket.org/arkadyb/grpc-gokit-greeter/pb"
	"bitbucket.org/arkadyb/grpc-gokit-greeter/service"
	grpctransport "github.com/go-kit/kit/transport/grpc"

	"google.golang.org/grpc"
)

func New(conn *grpc.ClientConn) service.Interface {
	var helloEndpoint = grpctransport.NewClient(
		conn, "pb.GreeterService", "SayHello",
		hello.EncodeGRPCSayHelloRequest,
		hello.DecodeGRPCSayHelloResponse,
		pb.SayHelloResponse{},
	).Endpoint()

	return endpoints.Endpoints{
		SayHelloEndpoint: helloEndpoint,
	}
}
