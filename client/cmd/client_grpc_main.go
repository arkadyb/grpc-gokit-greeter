package main

import (
	"flag"
	"fmt"
	"log"
	"time"

	grpcClient "bitbucket.org/arkadyb/grpc-gokit-greeter/client"
	"google.golang.org/grpc"

	"golang.org/x/net/context"
)

func main() {
	var (
		grpcAddr = flag.String("addr", ":8081",
			"gRPC address")
	)
	flag.Parse()
	ctx := context.Background()

	conn, err := grpc.Dial(*grpcAddr, grpc.WithInsecure(),
		grpc.WithTimeout(1*time.Second))

	if err != nil {
		log.Fatalln("gRPC dial:", err)
	}
	defer conn.Close()

	demoService := grpcClient.New(conn)

	args := flag.Args()
	var cmd string
	cmd, args = pop(args)
	msg, err := demoService.SayHello(ctx, cmd)
	if err != nil {
		log.Fatalln(err.Error())
	}

	fmt.Println(msg)
}

// parse command line argument one by one
func pop(s []string) (string, []string) {
	if len(s) == 0 {
		return "", s
	}
	return s[0], s[1:]
}
