package hello

import (
	"context"

	"bitbucket.org/arkadyb/grpc-gokit-greeter/pb"
)

type SayHelloRequest struct {
	Name string
}

//Encode and Decode Lorem Request
func EncodeGRPCSayHelloRequest(_ context.Context, r interface{}) (interface{}, error) {
	req := r.(SayHelloRequest)

	return &pb.SayHelloRequest{
		Name: req.Name,
	}, nil
}

func DecodeGRPCSayHelloRequest(_ context.Context, r interface{}) (interface{}, error) {
	req := r.(*pb.SayHelloRequest)

	return SayHelloRequest{
		Name: req.Name,
	}, nil
}
