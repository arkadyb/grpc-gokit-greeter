package hello

import (
	"context"

	"bitbucket.org/arkadyb/grpc-gokit-greeter/pb"
)

type SayHelloResponse struct {
	Message string
}

// Encode and Decode Lorem Response
func EncodeGRPCSayHelloResponse(_ context.Context, r interface{}) (interface{}, error) {
	resp := r.(SayHelloResponse)
	return &pb.SayHelloResponse{
		Message: resp.Message,
	}, nil
}

func DecodeGRPCSayHelloResponse(_ context.Context, r interface{}) (interface{}, error) {
	resp := r.(*pb.SayHelloResponse)
	return SayHelloResponse{
		Message: resp.Message,
	}, nil
}
