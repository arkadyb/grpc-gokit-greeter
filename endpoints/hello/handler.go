package hello

import (
	"context"

	"bitbucket.org/arkadyb/grpc-gokit-greeter/service"
	"github.com/go-kit/kit/endpoint"
)

func CreateEndpoint(s service.Interface) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(SayHelloRequest)

		greeting, err := s.SayHello(ctx, req.Name)
		if err != nil {
			return nil, err
		}

		return SayHelloResponse{Message: greeting}, nil
	}
}
