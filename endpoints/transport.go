package endpoints

import (
	"bitbucket.org/arkadyb/grpc-gokit-greeter/endpoints/hello"
	"bitbucket.org/arkadyb/grpc-gokit-greeter/pb"
	grpctransport "github.com/go-kit/kit/transport/grpc"
	"golang.org/x/net/context"
)

type grpcServer struct {
	hello grpctransport.Handler
}

// type DemoServiceServer interface {
// 	SayHello(context.Context, *HelloRequest) (*HelloResponse, error)
// }

// implement SemoServiceServer Interface in demo_service.pb.go
func (s *grpcServer) SayHello(ctx context.Context, r *pb.SayHelloRequest) (*pb.SayHelloResponse, error) {
	_, resp, err := s.hello.ServeGRPC(ctx, r)
	if err != nil {
		return nil, err
	}
	return resp.(*pb.SayHelloResponse), nil
}

// create new grpc server
func NewGRPCServer(_ context.Context, endpoint Endpoints) pb.GreeterServiceServer {
	return &grpcServer{
		hello: grpctransport.NewServer(
			endpoint.SayHelloEndpoint,
			hello.DecodeGRPCSayHelloRequest,
			hello.EncodeGRPCSayHelloResponse,
		),
	}
}
