package endpoints

import (
	"context"
	"errors"

	"github.com/go-kit/kit/endpoint"

	"bitbucket.org/arkadyb/grpc-gokit-greeter/endpoints/hello"
	"bitbucket.org/arkadyb/grpc-gokit-greeter/service"
)

//MakeServerEndpoints assigns requests handling to respective service end points
func MakeServerEndpoints(s service.Interface) (*Endpoints, error) {
	if s == nil {
		return nil, errors.New("Service instance cant be nil")
	}

	return &Endpoints{
		SayHelloEndpoint: hello.CreateEndpoint(s),
	}, nil
}

type Endpoints struct {
	SayHelloEndpoint endpoint.Endpoint
}

// Wrapping Endpoints as a Service implementation.
// Will be used in gRPC client
func (e Endpoints) SayHello(ctx context.Context, name string) (string, error) {
	req := hello.SayHelloRequest{
		Name: name,
	}

	resp, err := e.SayHelloEndpoint(ctx, req)
	if err != nil {
		return "", err
	}

	helloResp := resp.(hello.SayHelloResponse)
	return helloResp.Message, nil
}
