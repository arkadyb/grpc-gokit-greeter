package main

import (
	"flag"
	"fmt"
	"net"
	"os"
	"os/signal"
	"syscall"

	"google.golang.org/grpc"

	context "golang.org/x/net/context"

	"bitbucket.org/arkadyb/grpc-gokit-greeter/endpoints"
	"bitbucket.org/arkadyb/grpc-gokit-greeter/pb"
	"bitbucket.org/arkadyb/grpc-gokit-greeter/service"
)

func main() {
	var (
		gRPCAddr = flag.String("grpc", ":8081",
			"gRPC listen address")
	)
	flag.Parse()
	ctx := context.Background()

	// init greeter service
	var svc service.Interface
	svc = service.NewGreeterService()
	errChan := make(chan error)

	// creating Endpoints struct
	endpnts, err := endpoints.MakeServerEndpoints(svc)
	if err != nil {
		panic(err)
	}

	//execute grpc server
	go func() {
		listener, err := net.Listen("tcp", *gRPCAddr)
		if err != nil {
			errChan <- err
			return
		}
		handler := endpoints.NewGRPCServer(ctx, *endpnts)
		gRPCServer := grpc.NewServer()
		pb.RegisterGreeterServiceServer(gRPCServer, handler)
		errChan <- gRPCServer.Serve(listener)
	}()

	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errChan <- fmt.Errorf("%s", <-c)
	}()

	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errChan <- fmt.Errorf("%s", <-c)
	}()
	fmt.Println(<-errChan)
}
