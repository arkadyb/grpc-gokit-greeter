package service

import "context"

//Interface describes demo service main routines
type Interface interface {
	SayHello(ctx context.Context, name string) (string, error)
}
