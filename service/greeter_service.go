package service

import (
	"context"
	"errors"
	"fmt"
	"strings"
)

func NewGreeterService() Interface {
	srv := new(greeterService)
	return srv
}

type greeterService struct {
}

func (ds *greeterService) SayHello(ctx context.Context, name string) (string, error) {
	name = strings.TrimSpace(name)
	if len(name) == 0 {
		return "", errors.New("Name cant be empty")
	}
	return fmt.Sprintf("Hello %s", name), nil
}
